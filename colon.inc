%define begining 0

%macro colon 2
    %ifid %2
            %2: dq begining 
	        %else
		        %error "Second paramenter should be an identifier"
			    %endif
			        %ifstr %1
				        db %1, 0 
					    %else
					            %error 
						        %endif
							    %define begining %2
								%endmacro
