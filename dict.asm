%include "lib.inc"
%define BYTE 8

section .text

global find_word
find_word:
    .loop:
        add rsi, BYTE
        call string_equals
        sub rsi, BYTE
        test rax, rax 
        jnz .success
        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop
    .success:
        mov rax, rsi
        ret
